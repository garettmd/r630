# Dell R630 Support Files

Tools for configuring and supporting Dell R630 hardware from a Raspberry Pi device.

### Setup Steps for R630

Use the appropriate playbook for your procedure. These playbooks are meant to be run from one of the Raspberry Pi's connected to a frame, but really you can run from anywhere. The main thing that depends on the Pi/connected device is the `get_idrac_ips.py` which pulls IP addresses from the `dnsmasq` lease file. If you're not able to do that, you can just manually fill out a hosts file to use for your playbook run.

### Playbooks

- `compute_bios` - Does the main setup for a new r630, including NIC settings, grabbing version info and service tags, power settings, and creating vdisks. Uses `[idrac]` section in the `hosts` file by default.
- `bigswitch` - Does most of what `base_config` but with the addition of serial port settings and disabling the logical processor. Uses the `[bigswitch]` section in the `hosts` file by default.
- `upgrade_bios` - Upgrades the BIOS. Duh. Uses the `[idrac]` section in the `hosts` file by default.
- `reboot` - Reboots the server. Duh. Uses the `[idrac]` section in the `hosts` file by default.
- `gigamon` - Configures an r630 for installing the Gigamon IDS software. Uses the `[gigamon]` section in the `hosts` file by default.

### Running Playbooks

Before running a playbook, make sure you have your hosts set up correctly. If you're just using the `hosts` file, make sure you put the correct IPs in the section of the `hosts` file that your playbook will look at (see above). If you're connected to several r630s via the DHCP server running on a Pi, you can use the `get_idrac_ips.py` script to grab all the IP addresses for you, that way you don't have to worry about filling out the `hosts` file. To make sure you see the correct number of hosts, run `get_idrac_ips.py -c` to get the count of idrac hosts that the script sees.

Example for `base_config`:

```
ansible-playbook playbooks/base_config.yml -i get_idrac_ips.py --ask-vault-pass
```

Run the above command from the base directory of the repo. In the above example, the `-i get_idrac_ips.py` part runs a script that will pull a list of a frame's IPs from `dnsmasq`'s leases file and will supply that to the playbook.



##### Upgrade Bios

```
ansible-playbook playbooks/upgrade_bios.yml -i get_idrac_ips.py --ask-vault-pass
```

Run the above command from the base directory of the repo. You will then be prompted for the

 - Name of the bios upgrade file
 - The IP address for the raspberry pi (or any other NFS server you want to use)
 - The file path for the bios file (usually /files/nfs on the raspberry pi)
 - The nfs user (what you would use to login to the pi)
 - The nfs password (what you would use to login to the pi)



### Logs

Logs from actual runs of various scripts on the servers. Right now it's mainly logs from the `idrac_config.sh` script, which gets version and hardware info from iDRAC, configures some settings, then sets up a one-time PXE boot and reboots the server, which will then start the process of loading the appropriate OS. 



---

### Old Setup Steps Using Bash Scripts

These scripts were the old way of doing it. I've sinced moved to using Ansible playbooks (see above) which not only are a little more deterministic and are much easier to understand what's going on, also allow for *parallel* execution. Which is awesome. Runtime for the `idrac_config.sh` script on a standard 22 compute frame takes about 20 minutes. With the equivalent playbook, it takes more like 2 minutes.

1. `idrac_get_ips.sh` - Sets up text two text files: `<sitename>.txt` and `<sitename_info.txt>`. The former simply holds a list of IP addresses that other scripts can use to easily run commands on all the iDRAC servers. The latter holds the IP addresses along with service tags and MAC addresses of the iDRAC ports. This file is then moved into the `logs` directory to be added to version control.
2. `idrac_config.sh` - Sets several settings in iDRAC itself, ones that don't require any reboots to complete.
3. `idrac_enable_pxe.sh` - Enables PXE boot on LOM3. After the script reboots the server to make the change apply, the server *should* then initiate a PXE boot as it will not find any bootable items on the hard drive (the virtual disk) that was created by `idrac_config.sh`.
4. `idrac_set_boot_order.sh` - This is to set the permanent boot order to make the hard drive take precedence over other boot devices, so that we won't have to worry about the servers PXE booting on their own once they get onsite.
5. `idrac_set_vlan.sh` - This is essentially the "seal" script, in that it will set the iDRAC port to use vlan *##* (whichever you set it to), which will make iDRAC unreachable unless you set the network elements in the rack to use that vlan. **Make sure you perform any other miscellaneous steps on the servers before running this script as it will make iDRAC unreachable.** You should still be able to get to the OS you installed on the server earlier, but it's probably best practice to save this for last anyway.

### Scripts

- `idrac_config.sh` - Gets version and hardware info from iDRAC, configures some settings, then sets up a one-time PXE boot and reboots the server, which will then start the process of loading the appropriate OS
- `idrac_onetimeboot.sh` - A quick script to let you setup a one-time PXE boot an one or many servers. Specify the txt file with IP addresses as an argument
- `set_vlan.sh` - Enables vlans in iDRAC and sets it to the number you specify as an argument
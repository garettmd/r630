#!/usr/bin/env python

import subprocess
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-c', '--count', action='store_true',
                    required=False, dest='count')
parser.add_argument('--list', action='store_true', required=False, dest='list')
parser.set_defaults(count=False)
args = parser.parse_args()

bash_out = str(subprocess.Popen(
    "grep -E '64:00:6a:|74:e6:e2:|01:34:17:|84:7b:eb:|idrac' /var/lib/misc/dnsmasq.leases | awk '{print $3}'", shell=True, stdout=subprocess.PIPE).stdout.read())

servers = {
    'idrac': {
        'hosts': []
    },
    'local': {
        'hosts': ['127.0.0.1']
    }
}

if '\\n' in bash_out:
    bash_out_list = bash_out.split('\\n')
else:
    bash_out_list = bash_out.split('\n')

server_list = []

for line in bash_out_list:
    server_list.append(line.replace('\'', '').replace("b", ''))

try:
    server_list.remove('')
except ValueError:
    pass

for server in server_list:
    servers['idrac']['hosts'].append(server)

if args.list:
    print(servers)
else:
    print(len(servers['idrac']['hosts']))

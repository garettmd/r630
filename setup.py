from setuptools import setup
from sys import version_info, exit

if version_info < (3, 6):
    exit('Please use Python version 3.6 or higher')

setup(
    name='r630 tools',
    version='1.0',
    install_requires=[
        'bpython',
        'delegator.py'
    ]
)

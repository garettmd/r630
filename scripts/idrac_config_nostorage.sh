#!/bin/bash

if [ -z "$1" ]
then
    echo "Please specify the name of the log file and servers file"
    exit 1
elif [ -f "$1"_$(basename "$0" .sh).log ]
then
    echo "Filename already exists. Please choose a different one."
    exit 1
else
    site=$1
fi

if [ -z "$2" ]
then
    echo "Please specify the password used to login to iDRAC"
    exit 1
else
    pass=$2
fi

RACADM="./idrac.sh"
LOGFILE="$1"_$(basename "$0").log


for server in $(cat "$site".txt)
do
    echo "Starting iDRAC config for ${server}" >> "$LOGFILE"
    # disable default password warning
    $RACADM "$pass" "$server" "set idrac.tuning.DefaultCredentialWarning 0" >> "$LOGFILE"
    # get service tag
    $RACADM "$pass" "$server" "getsvctag" >> "$LOGFILE"
    # get idrac, BIOS and lifecycle controller version
    $RACADM "$pass" "$server" "getversion" >> "$LOGFILE"
    # get mac address of idrac port
    $RACADM "$pass" "$server" "get iDRAC.NIC.MACAddress" >> "$LOGFILE"
    # create virtual drive for OS install
    $RACADM "$pass" "$server" "storage createvd:RAID.Integrated.1-1 -rl r1 -pdkey:Disk.Bay.0:Enclosure.Internal.0-1:RAID.Integrated.1-1,Disk.Bay.1:Enclosure.Internal.0-1:RAID.Integrated.1-1 -name LINUX_OS" >> "$LOGFILE"
    # actually create it here
    $RACADM "$pass" "$server" "jobqueue create RAID.Integrated.1-1 --realtime" >> "$LOGFILE"
    # enable ipv6
    $RACADM "$pass" "$server" "set iDRAC.IPv6.Enable 1" >> "$LOGFILE"
    # set idrac port to LOM 3
    $RACADM "$pass" "$server" "set iDRAC.NIC.Selection 4" >> "$LOGFILE"
    # disable hot spare for PSUs
    $RACADM "$pass" "$server" "set System.Power.Hotspare.Enable 0" >> "$LOGFILE"
    # set redundancy policy to something
    $RACADM "$pass" "$server" "set System.Power.RedundancyPolicy 1" >> "$LOGFILE"
    echo " " >> "$LOGFILE"
done
#!/bin/bash

if [ -z "$1" ]
then
    echo "Please specify the name of the log file and servers file"
    exit 1
elif [ -f "$1"_$(basename "$0" .sh).log ]
then
    echo "Filename already exists. Please choose a different one."
    exit 1
else
    site=$1
fi

if [ -z "$2" ]
then
    echo "Please specify the password used to login to iDRAC"
    exit 1
else
    pass=$2
fi

RACADM="./idrac.sh"
LOGFILE="$1"_$(basename "$0").log


for server in $(cat "$site".txt)
do
    echo " " >> "$LOGFILE"
    echo "Starting iDRAC config for ${server}" >> "$LOGFILE"
    # get service tag
    $RACADM "$pass" "$server" "getsvctag" >> "$LOGFILE"
    # enable ipv6
    $RACADM "$pass" "$server" "set iDRAC.IPv6.Enable 1" >> "$LOGFILE"
    # set idrac port to LOM 3
    $RACADM "$pass" "$server" "set iDRAC.NIC.Selection 4" >> "$LOGFILE"
    # disable hot spare for PSUs
    $RACADM "$pass" "$server" "set System.Power.Hotspare.Enable 0" >> "$LOGFILE"
    # set redundancy policy to something
    $RACADM "$pass" "$server" "set System.Power.RedundancyPolicy 1" >> "$LOGFILE"
    $RACADM "$pass" "$server" "set BIOS.ProcSettings.LogicalProc Disabled" >> "$LOGFILE"
    $RACADM "$pass" "$server" "set BIOS.SerialCommSettings.SerialPortAddress Serial1Com1Serial2Com2" >> "$LOGFILE"
    $RACADM "$pass" "$server" "set BIOS.SerialCommSettings.SerialComm Auto" >> "$LOGFILE"
    $RACADM "$pass" "$server" "set BIOS.SerialCommSettings.ExtSerialConnector Serial2" >> "$LOGFILE"
    $RACADM "$pass" "$server" "set BIOS.SerialCommSettings.FailSafeBaud 19200" >> "$LOGFILE"
    $RACADM "$pass" "$server" "set BIOS.SerialCommSettings.ConTermType Vt100Vt220" >> "$LOGFILE"
    $RACADM "$pass" "$server" "set BIOS.SerialCommSettings.RedirAfterBoot Enabled" >> "$LOGFILE"
    $RACADM "$pass" "$server" "jobqueue create BIOS.Setup.1-1 -r pwrcycle -s TIME_NOW -e TIME_NA" >> "$LOGFILE"
    echo "Done configuring iDRAC for ${server}"
done
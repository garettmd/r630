#!/bin/bash

function update {
    ansible-playbook -i get_idrac_ips.py playbooks/upgrade_bios.yml --ask-vault-pass -f 4
}

echo "Starting upgrade process for R630 BIOS"
echo ""
echo "Number of R630s that will be updated:"

./get_idrac_ips.py -c

echo ""

while true; do
    read -p "Is that the correct number of R630s to be updated?" yn
    case $yn in
        [Yy]* ) update;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no";;
    esac
done

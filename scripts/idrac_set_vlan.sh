#!/bin/bash

if [ -z "$1" ]
then
    echo "Please specify the name of the log file and servers file"
    exit 1
elif [ -f "$1"_$(basename "$0" .sh).log ]
then
    echo "Filename already exists. Please choose a different one."
    exit 1
else
    site=$1
fi

if [ -z "$2" ]
then
    echo "Please specify the password used to login to iDRAC"
    exit 1
else
    pass=$2
fi

if [ -z "$3" ]
then
    echo "Please specify a vlan number"
    exit 1
fi

RACADM="./idrac.sh"
LOGFILE="$1"_$(basename "$0").log

for server in $(cat "$site".txt)
do
	$RACADM "$pass" "$server" "set iDRAC.NIC.VLanEnable 1" >> "$LOGFILE"
    $RACADM "$pass" "$server" "set iDRAC.NIC.VLanID ${3}" >> "$LOGFILE"
done

#!/bin/bash

USER=root
pass=$1
server=$2
shift
shift
cmd=$*

echo "$cmd"
sshpass -p "${pass}" ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=dev/null -o LogLevel=QUIET ${USER}@"${server}" racadm "${cmd}"
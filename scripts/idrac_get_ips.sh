#!/bin/bash

if [ -z "$1" ]
then
    echo "Please specify the name of the log file and servers file"
    exit 1
elif [ -f "$1"_info.txt ]
then
    echo "Filename already exists. Please choose a different one."
    exit 1
fi

grep idrac /var/lib/misc/dnsmasq.leases | awk '{print $2,$3,$4}' > "$1"_info.txt
grep idrac /var/lib/misc/dnsmasq.leases | awk '{print $3}' > "$1".txt
mv ./"$1"_info.txt ./logs
#!/usr/bin/env python3

import openpyxl
import argparse
import re
import delegator


parser = argparse.ArgumentParser()
parser.add_argument('-w', '--workbook', required=True, dest='workbook')
parser.add_argument('-s', '--servers', required=False, dest='servers')
args = parser.parse_args()

workbook = args.workbook
servers = args.servers

wb = openpyxl.load_workbook(workbook)
sheet = wb.get_sheet_by_name('Rack Sheet')
only_service_tag = re.compile('(idrac-)|(\\n)')

server_dic = {}
matching_rows = []
mac_ser_list = []
mac_ser_dic = {}

# Put list of servers from dnsmasq into a list of lists
if servers:
    with open(servers, 'r') as f:
        for line in f:
            line = line.split(' ')
            line[2] = only_service_tag.sub('', line[2])
            server_dic[line[0]] = line[2]
else:
    server_results = delegator.run(
        "sshpass -p Verizon1 ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=dev/null -o LogLevel=QUIET verizon@10.0.0.200 grep idrac /var/lib/misc/dnsmasq.leases | awk '{print $2,$3,$4}'")
    results_list = server_results.out.split('\n')
    if results_list[-1] == '':
        results_list.pop(-1)
    results_list.sort()
    for line in results_list:
        line = line.split(' ')
        line[2] = only_service_tag.sub('', line[2])
        server_dic[line[2]] = line[0]

# Make a list of all the spreadsheet rows that have R630s
for cell in sheet['B']:
    if 'dell r630' in str(cell.value).lower():
        matching_rows.append(cell.row)

# Make a dictionary from the matching_rows list that maps those rows'
# service tags to MAC addresses
for row in matching_rows:
    mac_ser_list.append([sheet.cell(row=row, column=3).value,
                         sheet.cell(row=row, column=5).value])
    mac_ser_list.sort()
    mac_ser_dic[sheet.cell(row=row, column=3).value] = sheet.cell(
        row=row, column=5).value

# Find differences between the two dictionaries
errors = dict(set(server_dic.items()) ^ set(mac_ser_dic.items()))

if errors:
    print('See the below discrepancies:')
    for key, value in errors.items():
        print('Service tag: {0}\t\tMAC Address: {1}'.format(key, value))
else:
    print('No errors!')

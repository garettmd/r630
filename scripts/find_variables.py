#!/usr/bin/env python

import argparse
import re

CHECK_MESSAGE = "The following variables need to be specified with the \"-p\" parameter flag structured like: \n" \
             "parameter1=value1 paremeter2=value2 \n\nUse these parameters with the 'cisco_setup_lines.py' file\n"

parser = argparse.ArgumentParser()
parser.add_argument('base_file')
args = parser.parse_args()
base_file = args.base_file


def find_variables(template):
    jinja_vars = set()
    with open(template, 'r') as f:
        data = f.readlines()
    pattern = re.compile('{{.+?}}')
    for line in data:
        for item in pattern.findall(line):
            jinja_vars.add(item)
    return jinja_vars


if __name__ == '__main__':
    variables = find_variables(base_file)
    print CHECK_MESSAGE
    for v in variables:
        v = re.sub('[{} ]', '', v)
        print v
